# App Settings & Materials (tutorials and intro to the app)

workdir_settings <-  fluidPage(

					useShinyjs(),

					extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

					titlePanel(h1(strong(em("General settings")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

					sidebarLayout(position = "left",

						sidebarPanel(

							tags$head(

								tags$script(src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript',includeScript("style/google-analytics.js"))

							),

							hr(),

							h3("Workflow"),

							p(style="text-align: justify;",'Save your work in a working directory'),
							tags$ul(style="font-family: 'times';font-size: 18px;",
							        tags$li("The directory will contain all outputs generated along your session."),
							        tags$li("The application will automatically select this directory as the working directory within a R session.")
							),

							br(),

							directoryInput(inputId='workflow_directory',
							               label = strong('Select a folder'),
							               value=if(nzchar(Sys.getenv("SHINY_BIOMOD_OUTPUT_DIR"))) Sys.getenv("SHINY_BIOMOD_OUTPUT_DIR") else "~"),

							hr()
						),

						mainPanel(
						  div(class="info", id="working_dir_info",style="border: 2px solid #e7f3fe; text-align: justify;",br(),
						      p(style="font-size: 16px;", shiny::span(icon('lightbulb',class="far fa-lightbulb",lib="font-awesome"),strong("Tip:"),"Setting",em("SHINY_BIOMOD_OUTPUT_DIR = <path to your working directory>"),"in your .Rprofile user file"),br(),
						        HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), "will select",em("<path to your working directory>"),"as your default working directory whenever you launch the application.",br(),
						        HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), "If you want to change this behaviour: ",strong("(1)")," Edit globally your .Rprofile or",strong("(2)"),actionLink(inputId="edit_R_profile_", label=strong("Edit locally your .Rprofile")),br(),br(),
						        HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), HTML('&nbsp;'), actionLink(inputId="tip_working_dir_got_it", label=strong("Got it !")))
						  )

						)
					)
				)
