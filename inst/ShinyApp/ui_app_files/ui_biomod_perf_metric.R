
ui_perf_metric <- fluidPage(

					useShinyjs(),

					extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),


					titlePanel(h1(strong(em("Performance metrics")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

					sidebarLayout(position = "left",

						sidebarPanel(

							tags$head(

								tags$script(src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript',includeScript("style/google-analytics.js"))

							),

							hr(),

							uiOutput('chooserPerfMetricSpecies'),

							#uiOutput("biomod_check_metrics_outputs")
							wellPanel(

							  selectizeInput(inputId='biomod_check_metrics_modeling_procedure_Outputs',

							                 label = "Select the BioMod procedure outputs you want to check :",

							                 choices = NULL,

							                 options=list(placeholder="Select your BioMod procedure")
							  ),

							  br(),

							  selectizeInput(inputId='biomod_check_metrics_modeling_id',

							                 label = "Select a modelling ID :",

							                 choices = NULL,#if(all(is.null(c(listOfBiomodModelingID(),listOfBiomodEnsModelingID())))) NULL else unique(c(listOfBiomodModelingID(),listOfBiomodEnsModelingID())),

							                 options=list(placeholder="Select your modelling ID")
							  ),

							  br(),

							  selectizeInput(inputId='biomod_check_metrics_models_Outputs',

							                 label = "Select model(s) outputs you want to check :",

							                 choices = NULL,

							                 #multiple=TRUE,

							                 options=list(placeholder="Select your model(s)")
							  ),

							  br(),

							  selectizeInput(inputId='biomod_check_metrics_evalmetric_Outputs',

							                 label = "Select evaluation metric outputs you want to check :",

							                 choices = NULL,

							                 multiple=TRUE,

							                 options=list(placeholder="Select your metric(s)")
							  ),

							  br(),

							  selectizeInput(inputId='biomod_check_metrics_evalrun_Outputs',

							                 label = "Select evaluation run(s) outputs you want to check :",

							                 choices = NULL,

							                 #multiple= TRUE,

							                 options=list(placeholder="Select your evaluation run(s)")
							  ),

							  br(),

							  selectizeInput(inputId='biomod_check_metrics_dataset_Outputs',

							                 label = "Select dataset(s) outputs you want to check :",

							                 choices = NULL,

							                 #multiple=TRUE,

							                 options=list(placeholder="Select your dataset(s)")
							  ),

							  awesomeCheckbox(inputId="biomod_check_metrics_check_only_active_tab", label="check current tab only", value = TRUE, status = "primary"),
							  helpText('Note: recommended when several species have been modelled with different parameters to avoid error messages'),

							  br(),

							  div(align='center',
							      actionButton(inputId="biomod_check_metrics_check_modeling",label = "Check",	icon = icon("check", lib = "font-awesome"), style = "primary")
							  )
							)

						),

						mainPanel(

							uiOutput("biomod_check_metrics_outputs_panelSpecies")
						)

					)
				)
