# Name: exportMap.py
# Description: Export multiple map documents as PDF 
# Requirements: ArcGIS Mapping Extension

# Import system modules
import os
import re
import arcpy
from arcpy import env
from arcpy.mapping import *

# Get command line arguments
import sys
args = sys.argv

# Define current working space 
env.workspace = args[1]

# Allow overwriting
env.overwriteOutput = True

# Set local variables
outdir		= args[2]

# Check out ArcGIS Mapping extension license
arcpy.CheckOutExtension('Mapping')

# create a list of map document in the workspace
list_mxd = []
for root, dirs, files in os.walk(env.workspace):
    for file in files:
		if file.endswith('_MapTemplate.mxd'):
			list_mxd.append(os.path.join(root, file))

PDFdoc = PDFDocumentCreate(outdir)

for map in	list_mxd:
	# read the original template 
	mxd = MapDocument(map)
	# output map
	outmap = os.path.join(os.path.dirname(os.path.dirname(map)),re.sub('_MapTemplate.mxd','.pdf',os.path.basename(map))).replace("\\","/")
	# export
	ExportToPDF(mxd,outmap)
	PDFdoc.appendPages(outmap)

# delete 
#del mxd

# save and close PDF document
PDFdoc.saveAndClose()

