# Name: AsciiToRaster.py
# Description: Convert ASCII raster to ESRI ARCINFO GRID raster
# Requirements: Spatial Analyst Extension

# Import system modules
import os
import re
import arcpy
from arcpy import env
from arcpy.sa import *

# Get command line arguments
import sys
args = sys.argv

# Define current working space 
env.workspace = args[1]

# Allow overwriting
env.overwriteOutput = True

# Set local variables
outdir		= args[2]
rasterType	= args[3]

# Check out ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension('Spatial')

# create a list of rasters in the workspace
list_rasters = arcpy.ListRasters("*","asc")

index = 0
for rast in list_rasters:
	
	# Execute ASCIIToRaster
	try:
		inrast = os.path.join(env.workspace,rast).replace("\\","/")
		outrast = re.sub('.asc','',os.path.join(outdir,rast).replace("\\","/"))
		arcpy.ASCIIToRaster_conversion(inrast, outrast, rasterType)  
	except:
		print	"...CONVERSION FAILED..."
	else:
		print	"...CONVERSION DONE..."
			
		# Delete ascii files		
		try:
			arcpy.Delete_management(rast) 	
		except:
			print	"...DELETING FAILED..."
		else:
			print	"...DELETING DONE..."
			
		# increment index	
		index = index + 1