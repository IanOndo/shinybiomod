# Sparse Principal Component Analysis

#---------------------------------------------------------------------------------

# Observer to update area for sparse pca analysis
observe({

	sparse_pca_data_from_area <- NULL

	if(!is.null(rasterLayers()) | !is.null(raster_worldclim_current()))
		sparse_pca_data_from_area <- c(sparse_pca_data_from_area, "All raster area" = "all_area")

	if(!is.null(myRegionRasters()))
		sparse_pca_data_from_area <- c("Your region area" = "area_of_interest")

	if(hasSavedBackground())
	  sparse_pca_data_from_area <- c(sparse_pca_data_from_area, "Your constrained area" = "area_constrained")

	updateSelectizeInput(session, inputId = "sparse_pca_data_from_area", choices = sparse_pca_data_from_area, selected = input$sparse_pca_data_from_area)

})

# Observer to update input$niche_space_area in the same time
observeEvent(input$sparse_pca_data_from_area,{
	if(!is.null(input$sparse_pca_data_from_area)){
		rasters$area <- input$sparse_pca_data_from_area
		updateSelectizeInput(session, inputId = "niche_space_area", selected = input$sparse_pca_data_from_area)
	}
})

# Observer to update environmental layers available for analysis
observe({

	sparse_pca_data_from_env_layers <- NULL

	if(!is.null(rasterLayers()))
		sparse_pca_data_from_env_layers <- c(sparse_pca_data_from_env_layers, "Your own raster layers" = "user_layers")

	if(!is.null(raster_worldclim_current()))
		sparse_pca_data_from_env_layers <- c(sparse_pca_data_from_env_layers, "WorldClim raster layers" = "wc_current_layers")

	updateSelectizeInput(session, inputId = "sparse_pca_data_from_env_layers", choices = sparse_pca_data_from_env_layers, selected = input$sparse_pca_data_from_env_layers)

})

# Observer to update input$niche_layers_from in the same time
observe({

	if(!is.null(input$sparse_pca_data_from_env_layers)){
		rasters$origins <- input$sparse_pca_data_from_env_layers
		updateSelectizeInput(session, inputId = "niche_layers_from", selected = input$sparse_pca_data_from_env_layers)
		#print(sprintf("input$niche_layers_from is :%s",input$niche_layers_from));flush.console()
	}
})

# Observer to update names of available variables for analysis
observe({
	input$sparse_pca_data_from_env_layers
	sparse_pca_data_from_explvar <- NULL
	if(!is.null(input$sparse_pca_data_from_env_layers) && !is.null(myRasterStack()) )
		sparse_pca_data_from_explvar <- c(sparse_pca_data_from_explvar, names(myRasterStack()))

	updateSelectizeInput(session, inputId = "sparse_pca_data_from_explvar", choices = sparse_pca_data_from_explvar, selected = sparse_pca_data_from_explvar)

})


# shows/hides sparse pca diagnostic output options
observeEvent(input$sparse_pca_show_diagnostics, ignoreNULL=TRUE, {
	if(input$sparse_pca_show_diagnostics){
		shinyjs::show('sparse_pca_diagnostic_options')
	}else
		shinyjs::hide('sparse_pca_diagnostic_options')
})

# keep selected variables in a reactive value
spca_explvar <- reactiveValues(old=NULL)


# Return a dataset to perfom a Sparse Principal Component Analysis with
data_spca <- reactive({
	sparse_pca_data <- myRasterStack()
	#input$sparse_pca_data_from_explvar
	dat_sparse_pca = NULL
	isolate({
		if(!is.null(sparse_pca_data) && !is.null(input$sparse_pca_data_from_explvar) && length(input$sparse_pca_data_from_explvar)>0L){#
			if(!is.null(input$sparse_pca_type) && input$sparse_pca_type=='Gram'){
				fn <- find_unique_filename('dat_spca_gram.txt')
				if(length(setdiff(isolate(spca_explvar$old),input$sparse_pca_data_from_explvar))==0 && file.exists(paste(dirname(rasterTmpFile()),fn,sep='/'))){
					dat <- subset(read.table(paste(dirname(rasterTmpFile()),fn,sep='/'), head=TRUE),subset=input$sparse_pca_data_from_explvar)
				}else{
					dat <- rasterToPoints(subset(sparse_pca_data,subset=input$sparse_pca_data_from_explvar))
					dat <- cov(dat[,-c(1,2)],use='pairwise.complete.obs')#suppressWarnings(dudi.pca(na.omit(dat[,-c(1:2)]), center=input$sparse_pca_centered, scale=input$sparse_pca_scaled, scannf=F, nf=input$sparse_pca_num_axis)$tab)
					write.table(dat,file=paste(dirname(rasterTmpFile()),fn,sep='/'),row.names=FALSE)
					# update the selected variables
					spca_explvar$old <- input$sparse_pca_data_from_explvar
				}
				dat_sparse_pca = dat
			}
			else if(!is.null(input$sparse_pca_type) && input$sparse_pca_type=='predictor'){
				fn <- find_unique_filename('dat_spca_predictor.txt')
				if(length(setdiff(isolate(spca_explvar$old),input$sparse_pca_data_from_explvar))==0 && file.exists(paste(dirname(rasterTmpFile()),fn,sep='/'))){
					dat <- subset(read.table(paste(dirname(rasterTmpFile()),fn,sep='/'), head=TRUE),subset=input$sparse_pca_data_from_explvar)
				}else{
					dat <- rasterToPoints(subset(sparse_pca_data,subset=input$sparse_pca_data_from_explvar))
					write.table(dat,file=paste(dirname(rasterTmpFile()),fn,sep='/'),row.names=FALSE)
					# update the selected variables
					spca_explvar$old <- input$sparse_pca_data_from_explvar
				}
				dat_sparse_pca = na.omit(dat[,-c(1:2)])
			}
			else
				return(NULL)
		}
		return(dat_sparse_pca)
	})
})


# Perform a Sparse Principal Component Analysis and return an 'spca' object
sparse_pca <- reactive({

	if(is.null(input$run_sparse_pca) || !input$run_sparse_pca>0)
		return(NULL)
	isolate({

		dat_sparse_pca = data_spca()
		sparse_acp = NULL
		if(!is.null(dat_sparse_pca)){

			sparse_acp = spca(x=dat_sparse_pca[,input$sparse_pca_data_from_explvar],

				K=input$sparse_pca_num_axis,

				type=input$sparse_pca_type,

				sparse=input$sparse_pca_sparsity,

				lambda=input$sparse_pca_lambda,

				para=eval(parse(text=paste0('c(',paste0('input$sparse_pca_nnz_axis_',1:input$sparse_pca_num_axis,collapse=', '),')'))),

				use.corr=input$sparse_pca_scaled,

				trace=FALSE,

				max.iter=input$sparse_pca_maxit,

				eps.conv=input$sparse_pca_epsconv
			)
		}
		return(sparse_acp)
	})
})

# sparsity_patterns <-reactive({

	# if(is.null(input$sparse_pca_num_axis))
		# return(NULL)

	# # compute the range size of input values
	# range_size 	= eval(parse(text=paste0('c(',paste0('input$sparse_pca_nnz_axis_',1:input$sparse_pca_num_axis,collapse=', '),')')))#eval(parse(text=paste0('c(',paste0('length(1',':input$sparse_pca_nnz_axis_',1:input$sparse_pca_num_axis,collapse=', '),')')))
	# arr 		= array(0,c(max(range_size),input$sparse_pca_num_axis))
	# colnames(arr) <- paste0('PC',1:input$sparse_pca_num_axis)

	# for(k in 1:input$sparse_pca_num_axis){
		# # get range of values for axis k
		# val = eval(parse(text=paste0('input$sparse_pca_nnz_axis_',k)))
		# # create the corresponding vector
		# vec = 1:val
		# len = max(range_size)-length(vec)
		# # recycle the last value of the vector to match the length of the longest vector
		# arr[,k] = append(vec,rep(val,len))
	# }

	# return(arr)
# })

output$sparse_pca_matrix_type <- renderUI({

	out = tagList()

	if(!is.null(input$sparse_pca_type) && nchar(input$sparse_pca_type)>0L &&  input$sparse_pca_type=='predictor'){

		out= tagList(out,

			#awesomeCheckbox(inputId="sparse_pca_centered", label="center", value = FALSE, status = "primary"),
			#helpText("A covariance matrix will be computed"),

			awesomeCheckbox(inputId="sparse_pca_scaled", label="scale", value = TRUE, status = "primary"),
			helpText("A correlation matrix will be computed")
		)
	}
	out
})
outputOptions(output,"sparse_pca_matrix_type", suspendWhenHidden=FALSE)

# Ui that allows the user to select a sparsity pattern ( i.e. number of non-zero elements for each principal component )
output$sparse_pca_sparsity_pattern <- renderUI({

	req(input$sparse_pca_num_axis)
	if(!is.null(input$sparse_pca_pev_sparsity) && !input$sparse_pca_pev_sparsity==TRUE)
		return()

	out = tagList()

	out = 	lapply(1:input$sparse_pca_num_axis,

				function(x){

					sliderInput(inputId=sprintf('sparse_pca_nnz_axis_%g',x),

						label=strong(sprintf("Select a number of non zero elements for PC%g:",x)),

						min=1,

						max=length(input$sparse_pca_data_from_explvar),

						value=ifelse(x>1, 2, length(input$sparse_pca_data_from_explvar)),#c(ceiling(length(input$sparse_pca_data_from_explvar)/2), length(input$sparse_pca_data_from_explvar)),

						step=1

					)
				}
			)
	out
})
outputOptions(output,"sparse_pca_sparsity_pattern", suspendWhenHidden=FALSE)

# Ui that allows the user to select one or more principal components for checking his (their) pev versus sparsity pattern
output$sparse_pca_sparsity_pattern_check_axis <- renderUI({

	req(input$sparse_pca_num_axis)
	if(!is.null(input$sparse_pca_pev_sparsity) && nchar(input$sparse_pca_pev_sparsity)>0L && !input$sparse_pca_pev_sparsity==TRUE)
		return()

	selectInput(inputId='sparse_pca_sparsity_pattern_check_axis',

		label=strong("Select one (or more) principal component(s)"),

		choices=paste0('PC',1:input$sparse_pca_num_axis),

		selected='PC1'

	)

})
outputOptions(output,"sparse_pca_sparsity_pattern_check_axis", suspendWhenHidden=FALSE)

# Ui that allows the user to select a sparsity pattern (number of non-zero elements) for each given principal component
output$sparse_pca_sparsity_pattern_check_nnz <- renderUI({

	if(!is.null(input$sparse_pca_pev_sparsity) && !input$sparse_pca_pev_sparsity==TRUE || is.null(input$sparse_pca_sparsity_pattern_check_axis) || nchar(input$sparse_pca_sparsity_pattern_check_axis)<1)
		return()

	out = tagList()

	out = 	lapply(1:input$sparse_pca_num_axis,#as.numeric(str_extract(input$sparse_pca_sparsity_pattern_check_axis,'[0-9]+')),

				function(x){

					numericInput(inputId=sprintf('sparse_pca_sparsity_pattern_check_nnz_axis_%g',x),

						label=sprintf("Select a number of non zero elements for PC%g:",x),

						value=eval(parse(text=paste0('input$sparse_pca_nnz_axis_',x))),

						min=eval(parse(text=paste0('input$sparse_pca_nnz_axis_',x))),

						max=length(input$sparse_pca_data_from_explvar),#	eval(parse(text=paste0('input$sparse_pca_nnz_axis_',x))),#eval(parse(text=paste0('input$sparse_pca_nnz_axis_',x,'[2]'))),

						step=1
					)
				}
			)
	out
})
outputOptions(output,"sparse_pca_sparsity_pattern_check_nnz", suspendWhenHidden=FALSE)

# Ui that allows the user to set parameters for a sparse pca analysis
output$sparse_pca_analysis <- renderUI({

	if(is.null(input$sparse_pca_data_from_explvar) || length(input$sparse_pca_data_from_explvar)<1){
		#showNotification(h4(strong("Upload your raster layers !!"),align='center'))
		return()
	}

	out = tagList()

		out= tagList(out,

			radioButtons(inputId="sparse_pca_type",

				label = strong('Type of data matrix:'),

				choices = c("PREDICTOR" = 'predictor', 'GRAM' = 'Gram'),

				selected = 'predictor',

				inline=TRUE

			),

			uiOutput('sparse_pca_matrix_type'),

			radioButtons(inputId='sparse_pca_sparsity',

				label = strong('Type of sparsity'),

				choices = c('Number of non zero loadings' = 'varnum', '1-norm penalty parameters' = 'penalty'),

				selected = 'varnum'

			),

			numericInput(inputId='sparse_pca_num_axis',

				label=strong("Number of principal components to keep :"),

				value = 2,

				min = 1,

				max = length(input$sparse_pca_data_from_explvar)

			),

			uiOutput('sparse_pca_sparsity_pattern'),

			numericInput(inputId='sparse_pca_lambda',

				label=strong("Quadratic penalty parameter (lambda)"),

				value = 1e-6,

				step = 1e-6
			),

			numericInput(inputId='sparse_pca_maxit',

				label=strong("Maximum number of iterations"),

				value = 200

			),

			numericInput(inputId='sparse_pca_epsconv',

				label=strong("Convergence criterion (epsilon)"),

				value =  0.001,

				step = 1e-4
			),

			tags$style(appCSS),

			useShinyjs(),

			div(align='center',

				withBusyIndicatorUI(
					actionButton(inputId="run_sparse_pca",

						label='Run Sparse PCA',

						style='primary'

					)
				)
			),

			busyIndicator("Computations in progress..",wait = 2000)
		)


	tagList(
		h3("Settings"),
		wellPanel(style="background-color: lavender",out),
		checkboxInput(inputId="sparse_pca_show_diagnostics", label=strong("show output options"), value=FALSE ),
		hr()
	)
})
#outputOptions(output,"sparse_pca_analysis", suspendWhenHidden=FALSE)

# Ui that allows the user to visualize outputs of the analysis
output$sparse_pca_analysis_outputs <- renderUI({

	#if(is.null(sparse_pca()))
		#return()

	if(is.null(input$run_sparse_pca) || (!is.null(input$run_sparse_pca) && !input$run_sparse_pca>0))
		return()

	if(is.null(input$sparse_pca_loadings) || is.null(input$sparse_pca_pev) || is.null(input$sparse_pca_pev_sparsity))
		return()

	myPanels = lapply(c("Loadings","Variances"),

		function(x){

			if(x=='Loadings' && input$sparse_pca_loadings==TRUE){

				tabPanel(title=x,icon=icon("table", lib="glyphicon"),tableOutput("sparse_pca_loadings_outputs"))

			}
			else if(x=="Variances" && (input$sparse_pca_pev==TRUE || input$sparse_pca_pev_sparsity==TRUE) ){

				out=tagList()

				if(input$sparse_pca_pev==TRUE)
					out=tagList(out, plotOutput("sparse_pca_pev_outputs", height=600, width='100%'))

				if(input$sparse_pca_pev_sparsity==TRUE)
					out=tagList(out, tags$style(type='text/css', '#sparse_pca_sparsity_pattern_report {background-color: rgba(255,255,255,0.90); border-style: none; }'),
						plotOutput("sparse_pca_pev_sparsity_outputs", height=600, width='100%'),
						verbatimTextOutput("sparse_pca_sparsity_pattern_report")
					)

				tabPanel(title=x,icon=icon("chart-bar", lib="glyphicon"),out)
			}

		}
	)
	myPanels <- myPanels[lengths(myPanels)>0L]
	do.call(tabsetPanel,myPanels)

})
outputOptions(output,"sparse_pca_analysis_outputs", suspendWhenHidden=FALSE)


# Observer to update input$sparse_pca_type
observeEvent(input$sparse_pca_type,{
	updateRadioButtons(session,'sparse_pca_type', selected=input$sparse_pca_type)
})

# Observer to update input$sparse_pca_sparsity
observeEvent(input$sparse_pca_sparsity,{
	updateRadioButtons(session,'sparse_pca_sparsity', selected=input$sparse_pca_sparsity)
})

# Observer to update input$sparse_pca_centered
observeEvent(input$sparse_pca_centered,{
	updateAwesomeCheckbox(session, "sparse_pca_centered", value = input$sparse_pca_centered)
})

# Observer to update input$sparse_pca_scaled
observeEvent(input$sparse_pca_scaled,{
	updateAwesomeCheckbox(session, "sparse_pca_scaled", value = input$sparse_pca_scaled)
})

# Observer to update input$sparse_pca_num_axis
observeEvent(input$sparse_pca_num_axis,{
	updateNumericInput(session, "sparse_pca_num_axis", value = input$sparse_pca_num_axis)
})

# Observer to update input$sparse_pca_loadings
observeEvent(input$sparse_pca_loadings,{
	updateAwesomeCheckbox(session, "sparse_pca_loadings", value = input$sparse_pca_loadings)
})

# Observer to update input$sparse_pca_pev
observeEvent(input$sparse_pca_pev,{
	updateAwesomeCheckbox(session, "sparse_pca_pev", value = input$sparse_pca_pev)
})

# Observer to update range of non zero elements for each principal component
observeEvent(

	ignoreNULL = TRUE,

	eventExpr = {
		if(!is.null(input$sparse_pca_num_axis))
			sum( eval(parse(text=paste0('c(',paste0('input$sparse_pca_nnz_axis_',1:input$sparse_pca_num_axis,collapse=', '),')'))) )# react as soon as the sum of nnz change (i.e. at least one of nnz of principal components has changed)
	},

	handlerExpr = {
		# condition prevents handler execution on initial app launch
		if (!is.null(input$run_sparse_pca) && input$run_sparse_pca > 0) {
			lapply(1:input$sparse_pca_num_axis,
				function(x){
					if(x>1)# number of non-zero element for axis > 1
						updateSliderInput(session, paste0('sparse_pca_nnz_axis_',x), value = min(eval(parse(text=paste0('input$sparse_pca_nnz_axis_',x))), input$sparse_pca_nnz_axis_1) )# need to be < number of non-zero element of axis 1
					else
						updateSliderInput(session, paste0('sparse_pca_nnz_axis_',x), value = eval(parse(text=paste0('input$sparse_pca_nnz_axis_',x))))
				}
			)
		}
	}
)

# Observer to update number of non zero elements for each principal component
observeEvent(

	ignoreNULL = TRUE,

	eventExpr = {
		if(!is.null(input$sparse_pca_num_axis)){
			sum( eval(parse(text=paste0('c(',paste0('input$sparse_pca_sparsity_pattern_check_nnz_axis_',1:input$sparse_pca_num_axis,collapse=', '),')'))) )
		}
	},

	handlerExpr = {
		# condition prevents handler execution on initial app launch
		if (!is.null(input$run_sparse_pca) && input$run_sparse_pca > 0) {
			lapply(1:input$sparse_pca_num_axis,
				function(x){
					updateNumericInput(session, paste0('sparse_pca_sparsity_pattern_check_nnz_axis_',x), value = eval(parse(text=paste0('input$sparse_pca_sparsity_pattern_check_nnz_axis_',x))))
				}
			)
		}
	}
)


# Observer to update input$sparse_pca_relative_loss
observeEvent(input$sparse_pca_relative_loss,{
	updateAwesomeCheckbox(session, "sparse_pca_relative_loss", value = input$sparse_pca_relative_loss)
})

values <- reactiveValues(sparse_pev=NULL,loadngs=NULL)

observeEvent(input$sparse_pca_data_from_explvar,{
	values$sparse_pev = NULL
	values$loadngs = NULL
})

observeEvent(

	ignoreNULL=TRUE,

	eventExpr={
		input$run_sparse_pca
	},

	handlerExpr={

		withBusyIndicatorServer(buttonId="run_sparse_pca",

			expr = {

				Sys.sleep(1)

				if(is.null(sparse_pca()))
					stop("Sparse PCA failed")

				if(is.null(input$run_sparse_pca) || (!is.null(input$run_sparse_pca) && !input$run_sparse_pca>0))
					stop("Sparse PCA failed")

				sparse_acp = sparse_pca()

				# Table of loadings of variables
				if(!is.null(input$sparse_pca_loadings) && input$sparse_pca_loadings){

					rownames(sparse_acp$loadings)<-NULL

					output$sparse_pca_loadings_outputs <- renderTable({
						data.frame(VARIABLES=sparse_acp$vn,sparse_acp$loadings)
					},striped=TRUE, width='50%')

				}

				# Plot of proportion of variance explained
				if(!is.null(input$sparse_pca_pev) && input$sparse_pca_pev){

					output$sparse_pca_pev_outputs <- renderPlot({
						# Evaluation of the proportion of variance explained by an axis
						pev = sparse_acp$pev

						## Representation of the proportion of variance explained
						bplt<-barplot(pev, ylab="PEV", cex.lab = 1.5, main="Proportion of explained variance", cex.main = 2, col=gray.colors(length(sparse_acp$pev)), names.arg=paste("PC ",1:length(sparse_acp$pev),sep=''))
						text(x=bplt, y=round(sparse_acp$pev,3) + 0.01, labels=round(sparse_acp$pev,3), xpd=TRUE)
					})
				}

				# Plot of proportion of variance explained versus sparsity
				if(!is.null(input$sparse_pca_pev_sparsity) && input$sparse_pca_pev_sparsity){

					# Dataset
					dat_sparse_pca = data_spca()
					dat_sparse_pca = dat_sparse_pca[,input$sparse_pca_data_from_explvar]

					# Define all possible sparsity patterns from inputs
					patterns =  permutations(n=length(input$sparse_pca_data_from_explvar),r=input$sparse_pca_num_axis,v=1:length(input$sparse_pca_data_from_explvar),repeats.allowed=T)
					colnames(patterns) <- paste0('PC',1:input$sparse_pca_num_axis)

					if(!is.null(dat_sparse_pca)){

						if(is.null(values$sparse_pev)){
							values$sparse_pev = array(0,c(nrow(patterns),ncol(patterns)))
							colnames(values$sparse_pev) <- paste0('PC',1:input$sparse_pca_num_axis)
						}
						if(is.null(values$loadngs)){
							values$loadngs = array(NA,c(length(input$sparse_pca_data_from_explvar),ncol(patterns),nrow(patterns)))
							dimnames(values$loadngs)[[1]] <- colnames(dat_sparse_pca)
							dimnames(values$loadngs)[[2]] <- paste0('PC',1:input$sparse_pca_num_axis)
						}

						pev = numeric(length=ncol(patterns))
						names(pev) <- paste0('PC',1:input$sparse_pca_num_axis)

						calc_time <- system.time({

							# Compute pev with an ordinary pca
							if(input$sparse_pca_relative_loss==TRUE){

								acp = spca(x=dat_sparse_pca,

									K=input$sparse_pca_num_axis,

									type=input$sparse_pca_type,

									sparse=input$sparse_pca_sparsity,

									lambda=input$sparse_pca_lambda,

									para=rep(length(input$sparse_pca_data_from_explvar),input$sparse_pca_num_axis),

									use.corr=input$sparse_pca_scaled,

									max.iter=input$sparse_pca_maxit,

									eps.conv=input$sparse_pca_epsconv
								)
								pev[] <- acp$pev
							}
							## Obtain the loadings of variables and the proportion of variance explained by each axis given a sparsity pattern

							# find the sparsity pattern (i.e. combination of non-zero loadings of each axis)
							target = eval(parse(text=paste0('c(',paste0('input$sparse_pca_nnz_axis_',1:input$sparse_pca_num_axis,collapse=', '),')')))
							k = apply(patterns,1,function(x) all(x==target))

							# if the pattern has not been calculated yet
							pattern_does_not_exist = all(isolate({values$sparse_pev[k,]})==0)
							if(pattern_does_not_exist){
								# do the computations
								spca_results =	isolate({spca(x=dat_sparse_pca,

									K=input$sparse_pca_num_axis,

									type=input$sparse_pca_type,

									sparse=input$sparse_pca_sparsity,

									lambda=input$sparse_pca_lambda,

									para=patterns[k,],

									use.corr=input$sparse_pca_scaled,

									max.iter=input$sparse_pca_maxit,

									eps.conv=input$sparse_pca_epsconv
								)
								})
								# store the results
								values$sparse_pev[k,] 	= spca_results$pev
								values$loadngs[,,k] 	= spca_results$loadings
							}

						})[3]

						# Plot the pev versus sparsity
						output$sparse_pca_pev_sparsity_outputs <- renderPlot({
							## Representation of the proportion of variance explained versus sparsity
							req(input$sparse_pca_sparsity_pattern_check_axis)
							idx = isolate({values$sparse_pev})[,input$sparse_pca_sparsity_pattern_check_axis]>0
							plt<-matplot(x=patterns[idx,input$sparse_pca_sparsity_pattern_check_axis], y=isolate({values$sparse_pev})[idx,input$sparse_pca_sparsity_pattern_check_axis],
							xlim=c(1,length(input$sparse_pca_data_from_explvar)),
							ylim=range(isolate({values$sparse_pev})[,input$sparse_pca_sparsity_pattern_check_axis]),
							type='b',
							ylab="PEV", xlab="Number of non zero loadings", pch=20, cex.lab = 1.5, main="Proportion of explained variance versus sparsity", cex.main = 1.5)

							## Highlight specific sparsity patterns for given:
							# principal components
							#pcomp = (1:input$sparse_pca_num_axis)[sapply(1:input$sparse_pca_num_axis,FUN=function(x) !identical(grep(pattern=x,input$sparse_pca_sparsity_pattern_check_axis),integer(0)))]
							# number of non zero element
							#nnz = eval(parse(text=paste0('c(',paste0('input$sparse_pca_sparsity_pattern_check_nnz_axis_', pcomp, collapse=', '),')')))
							new_target = eval(parse(text=paste0('c(',paste0('input$sparse_pca_sparsity_pattern_check_nnz_axis_',1:input$sparse_pca_num_axis, collapse=', '),')')))
							kk = apply(patterns,1,function(x) all(x==new_target))
							#ids = as.vector(mapply(FUN=function(x,y) match(x, table=patterns[,y], nomatch=0), nnz, input$sparse_pca_sparsity_pattern_check_axis)) # id of the sparsity pattern
							if(!all(isolate({values$sparse_pev[kk,]})==0)){
								points(x=patterns[kk, input$sparse_pca_sparsity_pattern_check_axis], y=isolate({values$sparse_pev})[kk, input$sparse_pca_sparsity_pattern_check_axis], pch=1,col='red',cex=5,lwd=3)
							}
							#legend()
							#text(x=bplt, y=round(sparse_acp$pev,2) + 2, labels=round(sparse_acp$pev,2), xpd=TRUE)
						})

						# Print report of analysis
						output$sparse_pca_sparsity_pattern_report <- renderPrint({

							if(is.null(input$sparse_pca_data_from_explvar) || is.null(patterns))
								return()

							message <- {

								cat('\t\t\t***********************************************************************************************************\n\n')
								cat(' \t\t\t\t\t\t\tSparse Principal Component Analysis (SPCA)\n\n')
								cat(' Selected number of principal components:',input$sparse_pca_num_axis,'\n\n')
								cat(' Sparsity constraint by:',ifelse(input$sparse_pca_sparsity=='varnum','number of sparse loadings','1-norm penalty parameters'),'\n\n')
								cat(' Quadratic penalty parameter (lambda):',input$sparse_pca_lambda,'\n\n')
								cat(' Maximum number of iteration:',input$sparse_pca_maxit,'\n\n')
								cat(' Convergence criterion (epsilon):',input$sparse_pca_epsconv,'\n\n')
								cat(' Selected variables for analysis are : \n\n')
								cat(' ',input$sparse_pca_data_from_explvar,'\n\n')
								cat('\t\t\t***********************************************************************************************************\n\n')
								cat(sprintf("%s\n* Performances of SPCA *\n%s\n\n",paste(rep("*",nchar("Performances of SPCA")+2*2),collapse=""),paste(rep("*",nchar("Performances of SPCA")+2*2),collapse="")))

								cat('SPCA has been performed on the first',input$sparse_pca_num_axis,'Principal Components using sparsity pattern:',
								paste('PC',1:input$sparse_pca_num_axis,':',patterns[k,],' ',sep=""),'\n\n')
								#eval(parse(text=paste0('c(',paste0('input$sparse_pca_sparsity_pattern_check_nnz_axis_',1:input$sparse_pca_num_axis, collapse=', '),')')))
								cat('Computation time:',as.vector(calc_time),'sec. (',as.vector(calc_time)/60,'min.) \n\n')

								if(length(input$sparse_pca_sparsity_pattern_check_axis)>0){

									new_target = eval(parse(text=paste0('c(',paste0('input$sparse_pca_sparsity_pattern_check_nnz_axis_',1:input$sparse_pca_num_axis, collapse=', '),')')))
									kk = apply(patterns,1,function(x) all(x==new_target))

									# if the pattern entered is not the pattern calculated AND has not been calculated before
									if(!all(patterns[k,]==new_target) &  all(isolate({values$sparse_pev[kk,]})==0)){

										cat('Sorry but the sparsity pattern you entered (',paste('PC',1:input$sparse_pca_num_axis,':',new_target,' ',sep=''),') has not been computed yet.\nPlease, select an existing pattern or run the sparse principal component with these numbers.\n\n')
										cat('-----------------------------------------------------------------------------------------------------------\n\n')
									}
									else{

										for(i in 1:input$sparse_pca_num_axis){
											# principal component
											pc = paste0('PC',i)
											# corresponding pattern of non zero loadings
											id = which(kk)
											#nnzl = eval(parse(text=paste0('input$sparse_pca_sparsity_pattern_check_nnz_axis_', i)))
											#id 	 = match(input$sparse_pca_sparsity_pattern_check_nnz_axis_1, table=patterns[,'PC1'], nomatch=0)
											#if(i>1){
												#id = max(match(nnzl, table=patterns[,pc], nomatch=0),id)
											#}
											# variables chosen + loadings
											#id_axis = match(nnzl, table=patterns[,pc], nomatch=0)
											#sparse_variables = names(isolate({values$loadngs})[, pc, id_axis])[as.vector(isolate({values$loadngs})[, pc, id_axis])!=0];
											#sparse_loadings = as.vector(isolate({values$loadngs})[, pc, id_axis])[as.vector(isolate({values$loadngs})[, pc, id_axis])!=0]
											sparse_variables = names(isolate({values$loadngs})[, pc, id])[as.vector(isolate({values$loadngs})[, pc, id])!=0]
											sparse_loadings = as.vector(isolate({values$loadngs})[, pc, id])[as.vector(isolate({values$loadngs})[, pc, id])!=0]
											# cumulative variance explained by sparse pca
											cum_sparse_pev = as.vector(cumsum(isolate({values$sparse_pev})[id, ])[pc])


											cat(sprintf(paste("%s\n* Sparse structure for",pc,"*\n%s\n\n"),paste(rep("*",nchar(paste("* Sparse structure for",pc,"*"))+2*2),collapse=""),paste(rep("*",nchar(paste("* Sparse structure for",pc,"*"))+2*2),collapse="")))
											cat(' Variables chosen (+ loadings):\n\n')
											cat('',sprintf('%s (%g)\n\n', sparse_variables, sparse_loadings))
											cat('\n\n')
											cat(' Number of non zero loadings:',patterns[kk, pc],'\n\n')
											cat(' Variance explained (%):',round(isolate({values$sparse_pev})[id, pc]*100,3),'\n\n')
											cat(' Cumulative variance explained (%):', round(cum_sparse_pev*100,3),'\n\n')
											if(input$sparse_pca_relative_loss==TRUE){

												if(all(pev==0)){
													cat('Sorry, but the relative loss of information option of your SPCA will only be available after running the SPCA with this option checked (i.e. you have to tick the checkbox)\n\n')
												}else{
													# cumulative variance explained by ordinary pca
													cum_pev = as.vector(cumsum(pev)[pc])
													cat(' Relative loss of information (%):',round((1-(isolate({values$sparse_pev})[id, pc]/pev[pc]))*100,3),'(from',pev[pc],'[ordinary PCA] to',isolate({values$sparse_pev})[id, pc],'[sparse PCA])\n\n')
													cat(' Cumulative (Total) loss of information (%):', round((1-(cum_sparse_pev/cum_pev))*100,3),'(from',cum_pev,'[ordinary PCA] to',cum_sparse_pev,'[sparse PCA])\n\n')
												}
											}
											cat('-----------------------------------------------------------------------------------------------------------\n\n')
										}
									}
								}
								else{
									cat("No Principal Component selected. Please, select one (or more) Principal Component'.\n\n")
									cat('-----------------------------------------------------------------------------------------------------------\n\n')
								}
								cat("For more informations about the analysis, please refer to tabs 'table' and 'details'.\n\n")
							}

						})
					}
				}
			}
		)
	}
)







